import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
//Redirect component
import UserContext from '../UserContext';

export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);

	//Clear the localStorage
	unsetUser()

	
	//By adding the useEffect, this will allow the Logout page to render first before trigerring the useEffect which changes the state of our user
	useEffect(() => {
		//Set the user state back into its original value
		setUser({id: null})
	}, [])

	return(

		<Navigate to="/" />
		)



}