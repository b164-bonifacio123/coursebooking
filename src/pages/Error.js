import { Container, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ErrorPage() {
	return(
		<Container>
			<h1 className="text-center text-danger">404 Page Not Found</h1>
			<Button variant="primary" as={Link} to="/">Go back home</Button>
		</Container>
	);
};