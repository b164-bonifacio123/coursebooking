const coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque exercitationem ut, natus adipisci ea nam alias, totam ratione in quasi hic repellendus, quod voluptatem praesentium explicabo laudantium nostrum! Veniam, cupiditate?",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python, Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque exercitationem ut, natus adipisci ea nam alias, totam ratione in quasi hic repellendus, quod voluptatem praesentium explicabo laudantium nostrum! Veniam, cupiditate?",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque exercitationem ut, natus adipisci ea nam alias, totam ratione in quasi hic repellendus, quod voluptatem praesentium explicabo laudantium nostrum! Veniam, cupiditate?",
		price: 55000,
		onOffer: true
	},



]

export default coursesData;